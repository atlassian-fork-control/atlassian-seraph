package com.atlassian.seraph.auth;

import com.atlassian.seraph.config.SecurityConfig;
import com.mockobjects.dynamic.C;
import com.mockobjects.dynamic.Mock;
import junit.framework.TestCase;

import javax.servlet.http.HttpServletRequest;

public class TestAuthTypeEnum extends TestCase
{
    public void testInvalidAuthTypeReturnsNone()
    {
        Mock request = new Mock(HttpServletRequest.class);
        Mock securityConfig = new Mock(SecurityConfig.class);

        securityConfig.expectAndReturn("getAuthType", "os_authType");
        request.expectAndReturn("getParameter", C.args(C.eq("os_authType")), "guest");

        final AuthType authType = AuthType.getAuthTypeInformation((HttpServletRequest) request.proxy(), (SecurityConfig) securityConfig.proxy());
        assertEquals(AuthType.NONE, authType);

        request.verify();
        securityConfig.verify();
    }
}
