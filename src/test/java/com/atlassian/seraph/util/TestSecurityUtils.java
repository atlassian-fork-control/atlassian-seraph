package com.atlassian.seraph.util;

import junit.framework.TestCase;

public class TestSecurityUtils extends TestCase
{
    public void testEncodeBasicAuthorizationCredentials() throws Exception
    {
        String basicAuthEncoded = SecurityUtils.encodeBasicAuthorizationCredentials("foo", "bar");
        // "foo:bar" encoded => "Zm9vOmJhcg=="
        assertEquals("Basic Zm9vOmJhcg==", basicAuthEncoded);
    }
    
    public void testDecodeBasicAuthorizationCredentials() throws Exception
    {
        // "foo:bar" encoded => "Zm9vOmJhcg=="
        SecurityUtils.UserPassCredentials credentials = SecurityUtils.decodeBasicAuthorizationCredentials("Basic Zm9vOmJhcg==");
        assertEquals("foo", credentials.getUsername());
        assertEquals("bar", credentials.getPassword());
    }
}
