package com.atlassian.seraph.service.rememberme;

import junit.framework.TestCase;

/**
 */
public class TestDefaultRememberMeToken extends TestCase
{
    public void testBuilder()
    {
        DefaultRememberMeToken.Builder builder = DefaultRememberMeToken.builder("randomString");
        assertNotNull(builder);

        final RememberMeToken token1 = builder.build();
        assertNotNull(token1);
        assertEquals("randomString", token1.getRandomString());
        assertNull(token1.getId());
        assertNull(token1.getUserName());
        assertEquals(0, token1.getCreatedTime());


        builder.setId(1234L).setUserName("userName").setCreatedTime(789L);
        final RememberMeToken token2 = builder.build();
        assertNotNull(token2);
        assertEquals("randomString", token2.getRandomString());
        assertEquals(new Long(1234L), token2.getId());
        assertEquals("userName", token2.getUserName());
        assertEquals(789L, token2.getCreatedTime());


        final RememberMeToken token3 = DefaultRememberMeToken.builder(token2).build();
        assertNotNull(token3);
        assertEquals("randomString", token3.getRandomString());
        assertEquals(new Long(1234L), token3.getId());
        assertEquals("userName", token3.getUserName());
        assertEquals(789L, token3.getCreatedTime());
    }
}
