package com.atlassian.seraph.service.rememberme;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.atlassian.seraph.config.SecurityConfig;

import junit.framework.TestCase;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 */
public class TestDefaultRememberMeConfiguration extends TestCase
{
    public static final int TWO_WEEKS = 2 * 7 * 24 * 60 * 60;
    private static final String COOKIE_NAME = "atlassian.seraph.remember.me";
    private static final String TOMCAT_WITHOUT_HTTPONLY_SUPPORT = "Apache Tomcat/6.0.18";

    private SecurityConfig securityConfig;
    private HttpServletRequest servletRequest;
    private HttpSession httpSession;
    private ServletContext servletContext;
    private DefaultRememberMeConfiguration configuration;

    @Override
    protected void setUp() throws Exception
    {
        securityConfig = mock(SecurityConfig.class);
        servletRequest = mock(HttpServletRequest.class);
        httpSession = mock(HttpSession.class);
        servletContext = mock(ServletContext.class);
        configuration = new DefaultRememberMeConfiguration(securityConfig);

        when(servletContext.getServerInfo()).thenReturn("Apache Tomcat/6.0.0");
        when(httpSession.getServletContext()).thenReturn(servletContext);
        when(servletRequest.getSession()).thenReturn(httpSession);
    }

    public void testDefaults()
    {
        when(securityConfig.getLoginCookieKey()).thenReturn(COOKIE_NAME);
        when(securityConfig.getLoginCookiePath()).thenReturn(null);
        when(securityConfig.isInsecureCookie()).thenReturn(false);

        assertEquals(COOKIE_NAME, configuration.getCookieName());
        assertEquals(false, configuration.isInsecureCookieAlwaysUsed());
        assertEquals(null, configuration.getCookieDomain(null));
        assertEquals(TWO_WEEKS, configuration.getCookieMaxAgeInSeconds());
        assertEquals(false, configuration.isCookieHttpOnly(servletRequest));
    }

    public void testSetPaths_NotSpecified_NoContext()
    {
        when(securityConfig.getLoginCookiePath()).thenReturn(null);
        when(servletRequest.getContextPath()).thenReturn("");

        assertEquals("/", configuration.getCookiePath(servletRequest));
    }

    public void testSetPaths_NotSpecified_RootContext()
    {
        when(securityConfig.getLoginCookiePath()).thenReturn(null);
        when(servletRequest.getContextPath()).thenReturn("/");

        assertEquals("/", configuration.getCookiePath(servletRequest));
    }

    public void testSetPaths_NotSpecified_WithContext()
    {
        when(securityConfig.getLoginCookiePath()).thenReturn(null);
        when(servletRequest.getContextPath()).thenReturn("/context");
        assertEquals("/context", configuration.getCookiePath(servletRequest));
    }

    public void testSetPaths_Specified()
    {
        when(securityConfig.getLoginCookiePath()).thenReturn("/specified");
        assertEquals("/specified", configuration.getCookiePath(servletRequest));
    }

    public void testIsCookieHttpOnlyForNonTomcatServer()
    {
        when(servletContext.getServerInfo()).thenReturn("Some Random Server/6.0.20");
        assertEquals(false, configuration.isCookieHttpOnly(servletRequest));
    }

    public void testIsCookieHttpOnlyForTomcat6019PlusServer()
    {
        when(servletContext.getServerInfo()).thenReturn("Apache Tomcat/6.0.20");
        assertEquals(true, configuration.isCookieHttpOnly(servletRequest));
    }

    public void testIsCookieHttpOnlyForTomcat6019Server()
    {
        when(servletContext.getServerInfo()).thenReturn("Apache Tomcat/6.0.19");
        assertEquals(true, configuration.isCookieHttpOnly(servletRequest));
    }

    public void testIsCookieHttpOnlyForTomcat6019MinusServer()
    {
        when(servletContext.getServerInfo()).thenReturn(TOMCAT_WITHOUT_HTTPONLY_SUPPORT);
        assertEquals(false, configuration.isCookieHttpOnly(servletRequest));
    }

    public void testIsCookieHttpOnlyForTomcat5528PlusServer()
    {
        when(servletContext.getServerInfo()).thenReturn("Apache Tomcat/5.5.29");
        assertEquals(true, configuration.isCookieHttpOnly(servletRequest));
    }

    public void testIsCookieHttpOnlyForTomcat5528Server()
    {
        when(servletContext.getServerInfo()).thenReturn("Apache Tomcat/5.5.28");
        assertEquals(true, configuration.isCookieHttpOnly(servletRequest));
    }

    public void testIsCookieHttpOnlyForTomcat5528MinusServer()
    {
        when(servletContext.getServerInfo()).thenReturn("Apache Tomcat/5.5.27");
        assertEquals(false, configuration.isCookieHttpOnly(servletRequest));
    }

    public void testIsCookieHttpOnlyForTomcat4Server()
    {
        when(servletContext.getServerInfo()).thenReturn("Apache Tomcat/4.0.0");
        assertEquals(false, configuration.isCookieHttpOnly(servletRequest));
    }

    public void testIsCookieHttpOnlyForServletApi3Servers()
    {
        //disregard server info when Servlet API is >=3
        when(servletContext.getServerInfo()).thenReturn(TOMCAT_WITHOUT_HTTPONLY_SUPPORT);
        when(servletContext.getMajorVersion()).thenReturn(3);
        assertEquals(true, configuration.isCookieHttpOnly(servletRequest));
    }
}
