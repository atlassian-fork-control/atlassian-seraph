package com.atlassian.seraph.filter;

import com.atlassian.seraph.auth.AuthType;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.controller.SecurityController;
import junit.framework.TestCase;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.security.Principal;
import javax.servlet.FilterChain;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.mockito.Mockito.*;

public class TestSecurityFilter extends TestCase
{
    @Mock private HttpServletRequest request;
    @Mock private HttpServletResponse response;
    @Mock private FilterChain filterChain;
    @Mock private SecurityConfig securityConfig;
    @Mock private SecurityController securityController;
    @Mock private Authenticator authenticator;
    @Mock private AuthenticationContext authenticationContext;
    private static final String OS_AUTH_TYPE = "os_authType";

    @Override
    protected void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);

        when(request.getAttribute("loginfilter.already.filtered")).thenReturn("not null"); // the actual value doesn't matter so long as it is not-null
        when(securityController.isSecurityEnabled()).thenReturn(true);

        when(securityConfig.getAuthType()).thenReturn(OS_AUTH_TYPE);
        when(securityConfig.getController()).thenReturn(securityController);
        when(securityConfig.getAuthenticator()).thenReturn(authenticator);
        when(securityConfig.getAuthenticationContext()).thenReturn(authenticationContext);

        super.setUp();
    }

    /**
     * When authType=basic and authentication fails it should abort. We are mocking out the part that
     * would normally send the 401 error so we can't really test that, thanks to Seraph's terrible internal
     * structure.
     */
    public void testAuthType_basic() throws Exception {
        final String authType = "basic";

        when(request.getServletPath()).thenReturn("/something");
        when(request.getParameter(OS_AUTH_TYPE)).thenReturn(authType);
        when(request.getQueryString()).thenReturn(String.format("%s=%s", OS_AUTH_TYPE, authType));
        when(request.getHeader("Authorization")).thenReturn("BASIC abcdef");
        when(authenticator.getUser(request, response)).thenReturn(null);

        final SecurityFilter filter = new TestableSecurityFilter();

        filter.doFilter(request, response, filterChain);

        // verify that nothing else happened.
        verify(filterChain, never()).doFilter(request, response);
        verify(authenticationContext, never()).setUser(Matchers.<Principal>any());
        verify(response, never()).sendRedirect(Matchers.<String>any());
    }

    public void testAuthType_cookie() throws Exception {
        final String authType = "cookie";

        when(request.getServletPath()).thenReturn("/something");
        when(request.getParameter(OS_AUTH_TYPE)).thenReturn(authType);
        when(request.getQueryString()).thenReturn(String.format("%s=%s", OS_AUTH_TYPE, authType));
        when(authenticator.getUser(request, response)).thenReturn(null);

        final SecurityFilter filter = new TestableSecurityFilter();

        filter.doFilter(request, response, filterChain);

        // check that we are getting a 401 with message.
        verify(response).sendError(401, "os_authType was 'cookie' but no valid cookie was sent.");

        // verify that nothing else happened.
        verify(filterChain, never()).doFilter(request, response);
        verify(authenticationContext, never()).setUser(Matchers.<Principal>any());
        verify(response, never()).sendRedirect(Matchers.<String>any());
    }

    // Just like in testAuthType_basic we can't test for the 401....
    public void testAuthType_any_basic() throws Exception
    {
        final String authType = "any";

        when(request.getServletPath()).thenReturn("/something");
        when(request.getParameter(OS_AUTH_TYPE)).thenReturn(authType);
        when(request.getQueryString()).thenReturn(String.format("%s=%s", OS_AUTH_TYPE, authType));

        // we sent along an authorization header triggering basic auth
        when(request.getHeader("Authorization")).thenReturn("BASIC abcdef");
        when(authenticator.getUser(request, response)).thenReturn(null);

        final SecurityFilter filter = new TestableSecurityFilter();

        filter.doFilter(request, response, filterChain);

        // verify that nothing else happened.
        verify(filterChain, never()).doFilter(request, response);
        verify(authenticationContext, never()).setUser(Matchers.<Principal>any());
        verify(response, never()).sendRedirect(Matchers.<String>any());
    }

    // test that sending a bad cookie doesn't log us in
    public void testAuthType_any_cookie() throws Exception
    {
        final String authType = "any";
        final Cookie[] cookies = { new Cookie("JSESSIONID", "value") };

        when(request.getSession(false)).thenReturn(null);
        when(request.getServletPath()).thenReturn("/something");
        when(request.getParameter(OS_AUTH_TYPE)).thenReturn(authType);
        when(request.getQueryString()).thenReturn(String.format("%s=%s", OS_AUTH_TYPE, authType));

        // we sent along a cookie
        when(request.getCookies()).thenReturn(cookies);

        when(authenticator.getUser(request, response)).thenReturn(null);

        final SecurityFilter filter = new TestableSecurityFilter();

        filter.doFilter(request, response, filterChain);

        // verify that nothing else happened.
        verify(filterChain, never()).doFilter(request, response);
        verify(authenticationContext, never()).setUser(Matchers.<Principal>any());
        verify(response, never()).sendRedirect(Matchers.<String>any());
    }

    // test that we are anonymous if we don't send a cookie at all
    public void testAuthType_any_anonymous() throws Exception
    {
        final String authType = "any";

        when(request.getServletPath()).thenReturn("/something");
        when(request.getParameter(OS_AUTH_TYPE)).thenReturn(authType);
        when(request.getQueryString()).thenReturn(String.format("%s=%s", OS_AUTH_TYPE, authType));

        // We don't have either a cookie or a basic auth header so we should go through as
        // anonymous
        when(authenticator.getUser(request, response)).thenReturn(null);

        final SecurityFilter filter = new TestableSecurityFilter();

        filter.doFilter(request, response, filterChain);

        // verify that we are actually anonymous and the rest of the filter chain got invoked
        verify(filterChain).doFilter(request, response);
        verify(authenticationContext).setUser(null);

        verify(response, never()).sendRedirect(Matchers.<String>any());
    }

    // If the request has an os_authTypeDefault parameter (presumably set by a filter that has run
    // before us in production) then that should be used as a default.
    public void testDefaults_Any() throws Exception
    {
        final Cookie[] cookies = { new Cookie("JSESSIONID", "value") };

        when(request.getAttribute(AuthType.DEFAULT_ATTRIBUTE)).thenReturn("any");
        when(request.getServletPath()).thenReturn("/rest/something");

        // we sent along a cookie
        when(request.getCookies()).thenReturn(cookies);

        when(authenticator.getUser(request, response)).thenReturn(null);

        final SecurityFilter filter = new TestableSecurityFilter();

        filter.doFilter(request, response, filterChain);

        // verify that nothing else happened. (since we sent an invalid cookie and are under /rest/)
        verify(filterChain, never()).doFilter(request, response);
        verify(authenticationContext, never()).setUser(Matchers.<Principal>any());
        verify(response, never()).sendRedirect(Matchers.<String>any());
    }

    // Ensure that if we get back an authType of NONE that we handle is properly
    public void testAuthType_none() throws Exception
    {
        when(request.getServletPath()).thenReturn("/something");
        // We don't have either a cookie or a basic auth header so we should go through as
        // anonymous
        when(authenticator.getUser(request, response)).thenReturn(null);

        final SecurityFilter filter = new TestableSecurityFilter();

        filter.doFilter(request, response, filterChain);

        // verify that we are actually anonymous and the rest of the filter chain got invoked
        verify(filterChain).doFilter(request, response);
        verify(authenticationContext).setUser(null);

        verify(response, never()).sendRedirect(Matchers.<String>any());
    }

    class TestableSecurityFilter extends SecurityFilter {
        @Override
        protected SecurityConfig getSecurityConfig()
        {
            return securityConfig;
        }
    }
}
