package com.atlassian.seraph.filter;

import com.atlassian.seraph.SecurityService;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.auth.RoleMapper;
import com.atlassian.seraph.config.DefaultRedirectPolicy;
import com.atlassian.seraph.config.RedirectPolicy;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.controller.SecurityController;
import com.atlassian.seraph.elevatedsecurity.ElevatedSecurityGuard;
import com.atlassian.seraph.elevatedsecurity.NoopElevatedSecurityGuard;
import com.atlassian.seraph.interceptor.Interceptor;
import com.atlassian.seraph.service.rememberme.RememberMeService;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 */
public class MockSecurityConfig implements SecurityConfig
{
    private final Authenticator authenticator;
    private final ElevatedSecurityGuard elevatedSecurityGuard;
    private final RoleMapper roleMapper;
    private final List interceptors;
    private final RedirectPolicy redirectPolicy;
    private SecurityController controller = new SecurityController()
    {
        @Override
        public boolean isSecurityEnabled()
        {
            return true;
        }

        @Override
        public void init(final Map<String, String> params, final SecurityConfig config)
        {
        }
    };

    public MockSecurityConfig()
    {
        this(NoopElevatedSecurityGuard.INSTANCE, null, null, Collections.EMPTY_LIST);
    }

    public MockSecurityConfig(final List interceptors)
    {
        this(null, null, null, interceptors);
    }

    public MockSecurityConfig(final ElevatedSecurityGuard elevatedSecurityGuard, final Authenticator authenticator, final RoleMapper roleMapper, final List interceptors)
    {
        this.elevatedSecurityGuard = elevatedSecurityGuard;
        this.authenticator = authenticator;
        this.roleMapper = roleMapper;
        this.interceptors = interceptors;
        this.redirectPolicy = new DefaultRedirectPolicy();
    }

    @Override
    public List<SecurityService> getServices()
    {
        return null;
    }

    @Override
    public String getLoginURL()
    {
        return null;
    }

    @Override
    public String getLoginURL(final boolean forUserRole, final boolean forPageCaps)
    {
        return null;
    }

    @Override
    public String getLoginForwardPath()
    {
        return null;
    }

    @Override
    public String getLinkLoginURL()
    {
        return null;
    }

    @Override
    public String getLogoutURL()
    {
        return null;
    }

    @Override
    public String getOriginalURLKey()
    {
        return "seraph_originalurl";
    }

    @Override
    public Authenticator getAuthenticator()
    {
        return authenticator;
    }

    @Override
    public AuthenticationContext getAuthenticationContext()
    {
        return null;
    }

    @Override
    public SecurityController getController()
    {
        return controller;
    }

    @Override
    public RoleMapper getRoleMapper()
    {
        return roleMapper;
    }

    @Override
    public ElevatedSecurityGuard getElevatedSecurityGuard()
    {
        return elevatedSecurityGuard;
    }

    @Override
    public RememberMeService getRememberMeService()
    {
        return null;
    }

    @Override
    public RedirectPolicy getRedirectPolicy()
    {
        return redirectPolicy;
    }

    @Override
    public <T extends Interceptor> List<T> getInterceptors(final Class<T> desiredInterceptorClass)
    {
        return interceptors;
    }

    @Override
    public String getLoginCookiePath()
    {
        return null;
    }

    @Override
    public String getLoginCookieKey()
    {
        return null;
    }

    @Override
    public String getWebsudoRequestKey()
    {
        return null;
    }

    @Override
    public void destroy()
    {
    }

    @Override
    public String getAuthType()
    {
        return null;
    }

    @Override
    public boolean isInsecureCookie()
    {
        return false;
    }

    @Override
    public int getAutoLoginCookieAge()
    {
        return 0;
    }

    @Override
    public List<String> getInvalidateSessionExcludeList()
    {
        return Collections.emptyList();
    }

    @Override
    public List<String> getInvalidateWebsudoSessionExcludeList()
    {
        return Collections.emptyList();
    }

    @Override
    public boolean isInvalidateSessionOnLogin()
    {
        return false;
    }

    @Override
    public boolean isInvalidateSessionOnWebsudo()
    {
        return false;
    }
}
