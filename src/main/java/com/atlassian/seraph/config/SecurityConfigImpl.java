package com.atlassian.seraph.config;

import com.atlassian.seraph.Initable;
import com.atlassian.seraph.SecurityService;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.seraph.auth.AuthenticationContextImpl;
import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.auth.RoleMapper;
import com.atlassian.seraph.controller.SecurityController;
import com.atlassian.seraph.elevatedsecurity.ElevatedSecurityGuard;
import com.atlassian.seraph.elevatedsecurity.NoopElevatedSecurityGuard;
import com.atlassian.seraph.interceptor.Interceptor;
import com.atlassian.seraph.ioc.ApplicationServicesRegistry;
import com.atlassian.seraph.service.rememberme.RememberMeService;
import com.atlassian.seraph.util.XMLUtils;

import com.opensymphony.util.ClassLoaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * The main implementation of Seraph's configuration - reads from seraph-config.xml.
 * <p>
 * This class is a Singleton, access it using SecurityConfigFactory.getInstance().
 */
public class SecurityConfigImpl implements Serializable, SecurityConfig
{
    private static final Logger log = LoggerFactory.getLogger(SecurityConfigImpl.class);

    public static final String DEFAULT_CONFIG_LOCATION = "seraph-config.xml";

    private static final int TWO_WEEKS_IN_SECONDS = Math.toIntExact(
            Duration.ofDays(14).getSeconds());

    private final Authenticator authenticator;
    private final ElevatedSecurityGuard elevatedSecurityGuard;
    private final RoleMapper roleMapper;
    private final SecurityController controller;
    private final List<SecurityService> services;
    private final List<Interceptor> interceptors = new CopyOnWriteArrayList<Interceptor>();

    private final String loginURL;
    private final String loginForwardPath;
    private final String logoutURL;
    private final String originalURLKey;
    private final String loginCookieKey;
    private final String linkLoginURL;
    private final String authType;
    private final String websudoRequestKey;
    private RedirectPolicy redirectPolicy;

    private boolean insecureCookie;
    private final boolean invalidateSessionOnLogin;
    private final boolean invalidateSessionOnWebsudo;
    private final List<String> invalidateSessionExcludeList;
    private final List<String> invalidateWebsudoSessionExcludeList;

    // the age of the auto-login cookie - default = 2 weeks (in seconds)
    private final int autoLoginCookieAge;

    private final LoginUrlStrategy loginUrlStrategy;
    private final String loginCookiePath;

    public SecurityConfigImpl(String configFileLocation) throws ConfigurationException
    {
        if (configFileLocation != null)
        {
            if (SecurityConfigImpl.log.isDebugEnabled())
            {
                SecurityConfigImpl.log.debug("Config file location passed.  Location: " + configFileLocation);
            }
        }
        else
        {
            configFileLocation = "seraph-config.xml";
            if (SecurityConfigImpl.log.isDebugEnabled())
            {
                SecurityConfigImpl.log.debug("Initialising securityConfig using default configFile: " + configFileLocation);
            }
        }

        try
        {
            final Element rootEl = loadConfigXml(configFileLocation);

            final NodeList nl = rootEl.getElementsByTagName("parameters");
            final Element parametersEl = (Element) nl.item(0);
            final Map<String, String> globalParams = getInitParameters(parametersEl);

            loginURL = globalParams.get("login.url");
            loginForwardPath = globalParams.get("login.forward.path");
            linkLoginURL = globalParams.get("link.login.url");
            logoutURL = globalParams.get("logout.url");
            loginCookiePath = globalParams.get("login.cookie.path");
            authType = globalParams.get("authentication.type");
            insecureCookie = "true".equals(globalParams.get("insecure.cookie"));

            if (globalParams.get("original.url.key") != null)
            {
                originalURLKey = globalParams.get("original.url.key");
            }
            else
            {
                originalURLKey = "seraph_originalurl";
            }

            if (globalParams.get("login.cookie.key") != null)
            {
                loginCookieKey = globalParams.get("login.cookie.key");
            }
            else
            {
                loginCookieKey = "seraph.os.cookie";
            }

            if (globalParams.get("websudo.request.key") != null)
            {
                websudoRequestKey = globalParams.get("websudo.request.key");
            }
            else
            {
                websudoRequestKey = "seraph.websudo.key";
            }

            if (globalParams.get("autologin.cookie.age") != null)
            {
                autoLoginCookieAge = Integer.parseInt(globalParams.get("autologin.cookie.age"));
            }
            else
            {
                autoLoginCookieAge = SecurityConfigImpl.TWO_WEEKS_IN_SECONDS;
            }

            if (globalParams.get("invalidate.session.on.websudo") != null)
            {
                invalidateSessionOnWebsudo = "true".equalsIgnoreCase(globalParams.get("invalidate.session.on.websudo"));
                if (globalParams.get("invalidate.websudo.session.exclude.list") != null)
                {
                    final String[] excludes = globalParams.get("invalidate.websudo.session.exclude.list").split(",");
                    invalidateWebsudoSessionExcludeList = Arrays.asList(excludes);
                }
                else
                {
                    invalidateWebsudoSessionExcludeList = Collections.emptyList();
                }
            }
            else
            {
                invalidateSessionOnWebsudo = false;
                invalidateWebsudoSessionExcludeList = Collections.emptyList();
            }

            if (globalParams.get("invalidate.session.on.login") != null)
            {
                invalidateSessionOnLogin = "true".equalsIgnoreCase(globalParams.get("invalidate.session.on.login"));
                if (globalParams.get("invalidate.session.exclude.list") != null)
                {
                    final String[] excludes = globalParams.get("invalidate.session.exclude.list").split(",");
                    invalidateSessionExcludeList = Arrays.asList(excludes);
                }
                else
                {
                    invalidateSessionExcludeList = Collections.emptyList();
                }
            }
            else
            {
                invalidateSessionOnLogin = false;
                invalidateSessionExcludeList = Collections.emptyList();
            }

            // be VERY careful about changing order here, THIS reference is passed out while initializing and so we are partially constructed when
            // clients call us

            authenticator = configureAuthenticator(rootEl);
            controller = configureController(rootEl);
            roleMapper = configureRoleMapper(rootEl);
            services = Collections.unmodifiableList(configureServices(rootEl));
            configureInterceptors(rootEl);
            loginUrlStrategy = configureLoginUrlStrategy(rootEl);
            configureRedirectPolicy(rootEl);
            elevatedSecurityGuard = configureElevatedSecurityGuard(rootEl);
        }
        catch (SAXException e)
        {
            throw new ConfigurationException("Exception configuring from '" + configFileLocation + "'.", e);
        }
        catch (IOException e)
        {
            throw new ConfigurationException("Exception configuring from '" + configFileLocation + "'.", e);
        }
        catch (ParserConfigurationException e)
        {
            throw new ConfigurationException("Exception configuring from '" + configFileLocation + "'.", e);
        }
    }

    private Element loadConfigXml(final String configFileLocation)
            throws SAXException, IOException, ParserConfigurationException
    {
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        final URL fileUrl = ClassLoaderUtil.getResource(configFileLocation, getClass());

        if (fileUrl == null)
        {
            throw new IllegalArgumentException("No such XML file: " + configFileLocation);
        }

        // Parse document
        final Document doc = factory.newDocumentBuilder().parse(fileUrl.toString());
        return doc.getDocumentElement();
    }

    protected void configureRedirectPolicy(final Element rootEl) throws ConfigurationException
    {
        // If no redirect-policy is configured, then this will be null which is valid.
        redirectPolicy = (RedirectPolicy) configureClass(rootEl, "redirect-policy", this);

        // If none is configured, then use the default.
        if (redirectPolicy == null)
        {
            redirectPolicy = new DefaultRedirectPolicy();
        }
    }

    private LoginUrlStrategy configureLoginUrlStrategy(final Element rootEl) throws ConfigurationException
    {
        LoginUrlStrategy loginUrlStrategy = (LoginUrlStrategy) configureClass(rootEl, "login-url-strategy", this);

        if (loginUrlStrategy == null)
        {
            loginUrlStrategy = new DefaultLoginUrlStrategy();
        }
        return loginUrlStrategy;
    }

    private Authenticator configureAuthenticator(final Element rootEl) throws ConfigurationException
    {
        Authenticator authenticator = (Authenticator) configureClass(rootEl, "authenticator", this);

        if (authenticator == null)
        {
            // We used to try to load the DefaultAuthenticator here, but it is now abstract so we can only fail.
            throw new ConfigurationException("No authenticator implementation was configured in SecurityConfig.");
        }
        return authenticator;
    }

    private ElevatedSecurityGuard configureElevatedSecurityGuard(final Element rootEl) throws ConfigurationException
    {
        ElevatedSecurityGuard elevatedSecurityGuard = (ElevatedSecurityGuard) configureClass(rootEl, "elevatedsecurityguard", this);
        if (elevatedSecurityGuard == null)
        {
            elevatedSecurityGuard = NoopElevatedSecurityGuard.INSTANCE;
        }
        return elevatedSecurityGuard;
    }

    private SecurityController configureController(final Element rootEl) throws ConfigurationException
    {
        SecurityController controller = (SecurityController) configureClass(rootEl, "controller", this);

        try
        {
            if (controller == null)
            {
                controller = (SecurityController) ClassLoaderUtil.loadClass(SecurityController.NULL_CONTROLLER, getClass()).newInstance();
            }
        }
        catch (final Exception e)
        {
            throw new ConfigurationException("Could not lookup class: " + SecurityController.NULL_CONTROLLER, e);
        }
        return controller;
    }

    private RoleMapper configureRoleMapper(final Element rootEl) throws ConfigurationException
    {
        return (RoleMapper) configureClass(rootEl, "rolemapper", this);
    }

    private static Initable configureClass(final Element rootEl, final String tagname, final SecurityConfig owner)
            throws ConfigurationException
    {
        final NodeList elementList = rootEl.getElementsByTagName(tagname);

        if (elementList.getLength() == 0)
        {
            return null;
        }
        final Element authEl = (Element) elementList.item(0);
        final String clazz = authEl.getAttribute("class");
        if (clazz == null || clazz.trim().length() == 0)
        {
            return null;
        }

        final Initable initable;
        try
        {
            initable = (Initable) ClassLoaderUtil.loadClass(clazz, owner.getClass()).newInstance();
        }
        catch (InstantiationException ex)
        {
            // Java's has a null message - add something useful
            throw new ConfigurationException("Unable to instantiate class '" + clazz + "'", ex);
        }
        catch (Exception ex)
        {
            final String message = "Unable to load " + tagname + " class '" + clazz + "': " + ex.getMessage();
            // SER-173: log before rethrow, else the interesting part of the stacktrace is buried too deep and gets discarded
            log.error(message, ex);
            throw new ConfigurationException(message, ex);
        }

        try
        {
            initable.init(getInitParameters(authEl), owner);
            return initable;
        }
        catch (Exception ex)
        {
            final String message = "Error caught in initialisation of " + tagname + " class '" + clazz + "': " + ex.getMessage();
            // SER-173: log before rethrow, else the interesting part of the stacktrace is buried too deep and gets discarded or ignored
            log.error(message, ex);
            throw new ConfigurationException(message, ex);
        }
    }

    // only called from the constructor
    private List<SecurityService> configureServices(final Element rootEl) throws ConfigurationException
    {
        final NodeList nl = rootEl.getElementsByTagName("services");
        final List<SecurityService> result = new ArrayList<SecurityService>();

        if ((nl != null) && (nl.getLength() > 0))
        {
            final Element servicesEl = (Element) nl.item(0);
            final NodeList serviceList = servicesEl.getElementsByTagName("service");

            for (int i = 0; i < serviceList.getLength(); i++)
            {
                final Element serviceEl = (Element) serviceList.item(i);
                final String serviceClazz = serviceEl.getAttribute("class");

                if ((serviceClazz == null) || "".equals(serviceClazz))
                {
                    throw new ConfigurationException("Service element with bad class attribute");
                }

                try
                {
                    SecurityConfigImpl.log.debug("Adding seraph service of class: " + serviceClazz);
                    final SecurityService service = (SecurityService) ClassLoaderUtil.loadClass(serviceClazz, getClass()).newInstance();

                    service.init(getInitParameters(serviceEl), this);

                    result.add(service);
                }
                catch (final Exception e)
                {
                    throw new ConfigurationException("Could not getRequest service: " + serviceClazz, e);
                }
            }
        }
        return result;
    }

    private void configureInterceptors(final Element rootEl) throws ConfigurationException
    {
        final NodeList nl = rootEl.getElementsByTagName("interceptors");

        if ((nl != null) && (nl.getLength() > 0))
        {
            final Element interceptorsEl = (Element) nl.item(0);
            final NodeList interceptorList = interceptorsEl.getElementsByTagName("interceptor");

            for (int i = 0; i < interceptorList.getLength(); i++)
            {
                final Element interceptorEl = (Element) interceptorList.item(i);
                final String interceptorClazz = interceptorEl.getAttribute("class");

                if ((interceptorClazz == null) || "".equals(interceptorClazz))
                {
                    throw new ConfigurationException("Interceptor element with bad class attribute");
                }

                try
                {
                    SecurityConfigImpl.log.debug("Adding interceptor of class: " + interceptorClazz);
                    final Interceptor interceptor = (Interceptor) ClassLoaderUtil.loadClass(interceptorClazz, getClass()).newInstance();

                    interceptor.init(getInitParameters(interceptorEl), this);

                    interceptors.add(interceptor);
                }
                catch (final Exception e)
                {
                    throw new ConfigurationException("Could not getRequest service: " + interceptorClazz, e);
                }
            }
        }
    }

    /**
     * Returns a Map of the "init-param" properties under the given element. The map could be empty, but is guaranteed
     * not to be null.
     *
     * @param el The XML config element.
     *
     * @return a Map of the "init-param" properties under the given element.
     */
    private static Map<String, String> getInitParameters(final Element el)
    {
        final Map<String, String> params = new HashMap<String, String>();
        final NodeList nl = el.getElementsByTagName("init-param");

        for (int i = 0; i < nl.getLength(); i++)
        {
            final Node initParam = nl.item(i);
            final String paramName = XMLUtils.getContainedText(initParam, "param-name");
            final String paramValue = XMLUtils.getContainedText(initParam, "param-value");
            params.put(paramName, paramValue);
        }
        return Collections.unmodifiableMap(params);
    }

    @Override
    public void destroy()
    {
        for (final Object element : services)
        {
            final SecurityService securityService = (SecurityService) element;
            securityService.destroy();
        }

        for (final Object element : interceptors)
        {
            ((Interceptor) element).destroy();
        }
    }

    /**
     * Do not use in production! Only used in tests, will be removed.
     *
     * @param interceptor the Interceptor to add
     */
    public void addInterceptor(final Interceptor interceptor)
    {
        interceptors.add(interceptor);
    }

    @Override
    public List<SecurityService> getServices()
    {
        return services;
    }

    @Override
    public String getLoginURL()
    {
        return getLoginURL(false, false);
    }

    public String getLoginURL(final boolean forUserRole, final boolean forPageCaps)
    {
        String loginUrl = loginUrlStrategy.getLoginURL(this, loginURL);

        if (!forUserRole)
        {
            loginUrl = loginUrl.replaceAll("\\$\\{userRole\\}", "");
        }

        if (!forPageCaps)
        {
            loginUrl = loginUrl.replaceAll("\\$\\{pageCaps\\}", "");
        }

        return loginUrl;
    }

    @Override
    public String getLoginForwardPath()
    {
        return loginForwardPath;
    }

    @Override
    public String getLinkLoginURL()
    {
        return loginUrlStrategy.getLinkLoginURL(this, linkLoginURL);
    }

    @Override
    public String getLogoutURL()
    {
        return loginUrlStrategy.getLogoutURL(this, logoutURL);
    }

    @Override
    public String getOriginalURLKey()
    {
        return originalURLKey;
    }

    @Override
    public Authenticator getAuthenticator()
    {
        return authenticator;
    }

    @Override
    public AuthenticationContext getAuthenticationContext()
    {
        return new AuthenticationContextImpl();
    }

    @Override
    public SecurityController getController()
    {
        return controller;
    }

    @Override
    public RoleMapper getRoleMapper()
    {
        return roleMapper;
    }

    @Override
    public RedirectPolicy getRedirectPolicy()
    {
        return redirectPolicy;
    }

    @Override
    public <T extends Interceptor> List<T> getInterceptors(final Class<T> desiredInterceptorClass)
    {
        final List<T> result = new ArrayList<T>();
        for (final Interceptor interceptor : interceptors)
        {
            if (desiredInterceptorClass.isAssignableFrom(interceptor.getClass()))
            {
                result.add(desiredInterceptorClass.cast(interceptor));
            }
        }
        return Collections.unmodifiableList(result);
    }

    @Override
    public String getLoginCookiePath()
    {
        return loginCookiePath;
    }

    @Override
    public String getLoginCookieKey()
    {
        return loginCookieKey;
    }

    @Override
    public String getWebsudoRequestKey()
    {
        return websudoRequestKey;
    }

    @Override
    public String getAuthType()
    {
        return authType;
    }

    @Override
    public boolean isInsecureCookie()
    {
        return insecureCookie;
    }

    @Override
    public int getAutoLoginCookieAge()
    {
        return autoLoginCookieAge;
    }

    @Override
    public ElevatedSecurityGuard getElevatedSecurityGuard()
    {
        return elevatedSecurityGuard;
    }

    /**
     * @return a NON NULL RememberMeService implementation
     */
    @Override
    public RememberMeService getRememberMeService()
    {
        return ApplicationServicesRegistry.getRememberMeService();
    }

    @Override
    public boolean isInvalidateSessionOnLogin()
    {
        return invalidateSessionOnLogin;
    }

    @Override
    public boolean isInvalidateSessionOnWebsudo()
    {
        return invalidateSessionOnWebsudo;
    }

    @Override
    public List<String> getInvalidateSessionExcludeList()
    {
        return invalidateSessionExcludeList;
    }

    @Override
    public List<String> getInvalidateWebsudoSessionExcludeList()
    {
        return invalidateWebsudoSessionExcludeList;
    }

}
